import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import fa from './components/globals/Fa.vue';
import VueLodash from 'vue-lodash';
import lodash from 'lodash';
import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/animations/scale.css';


Vue.use(VueLodash, { name: 'custom', lodash: lodash });

Vue.config.productionTip = false;

Vue.directive('tippy', { 
  inserted(el, binding) {
    tippy(el, {
      content: binding.value,
      placement: 'bottom',
      animation: 'scale',
    });
  },
});


Vue.component('fa', fa);
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');


